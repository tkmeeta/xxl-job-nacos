package com.xxl.job.core.config;

import com.xxl.job.core.condition.NacosPropertyCondition;
import com.xxl.job.core.discovery.DiscoveryProcessor;
import com.xxl.job.core.discovery.NacosDiscoveryProcessor;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Conditional;

/**
 * registry center auto configuration calss
 * @author YIJIUE
 */
public class DiscoveryAutoConfiguration {

    @Bean
    @Conditional(value = NacosPropertyCondition.class)
    public DiscoveryProcessor initNacos() {
        return new NacosDiscoveryProcessor();
    }
}
